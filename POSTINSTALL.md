<sso>

This app integrates with Cloudron authentication. Cloudron users can login and use ownCloud.

However, _admin_ status of Cloudron user is not carried over to ownCloud. For this reason,
this app comes with an pre-setup admin user. This admin user can grant admin previleges to
other users.

</sso>

The admin credentials are:

`username: admin`

`password: changeme`

**Please change the admin password on first login**
