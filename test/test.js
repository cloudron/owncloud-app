#!/usr/bin/env node

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    request = require('request'),
    superagent = require('superagent'),
    util = require('util'),
    manifest = require('../CloudronManifest.json'),
    webdriver = require('selenium-webdriver');

var by = require('selenium-webdriver').By,
    until = require('selenium-webdriver').until;

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    var chrome = require('selenium-webdriver/chrome');
    var server, browser = new chrome.Driver();

    var TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 30000;
    var LOCATION = 'test';
    var LOCAL_FILENAME = 'sticker.png';
    var REMOTE_FILENAME = 'sticker.png';
    var CONTACT_NAME = 'Johannes';

    var app;
    var username = process.env.USERNAME;
    var password = process.env.PASSWORD;
    var userId;
    var adminUser = 'admin';
    var adminPassword = 'changeme';

    before(function (done) {
        if (!process.env.USERNAME) return done(new Error('USERNAME env var not set'));
        if (!process.env.PASSWORD) return done(new Error('PASSWORD env var not set'));

        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();

        done();
    });

    after(function (done) {
        browser.quit();
        server.stop();
        done();
    });

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];

        expect(app).to.be.an('object');
    }

    function testFileDownload(callback) {
        var data = {
            url: util.format('https://%s/remote.php/webdav/%s', app.fqdn, REMOTE_FILENAME),
            auth: { username: username, password: password },
            encoding: 'binary'
        };

        request.get(data, function (error, response, body) {
            if (error !== null) return callback(error);
            if (response.statusCode !== 200) return callback('Status code: ' + response.statusCode);
            if (body !== fs.readFileSync(path.resolve(LOCAL_FILENAME)).toString('binary')) return callback('File corrupt');

            callback(null);
        });
    }

    function login(username, password, callback) {
        browser.get('https://' + app.fqdn).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//input[@name="user"]')), TEST_TIMEOUT);
        }).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(by.xpath('//input[@name="user"]'))), TEST_TIMEOUT);
        }).then(function () {
            return browser.sleep(5000);
        }).then(function () {
            return browser.findElement(by.xpath('//input[@name="user"]')).sendKeys(username);
        }).then(function () {
            return browser.findElement(by.xpath('//input[@name="password"]')).sendKeys(password);
        }).then(function () {
            return browser.findElement(by.tagName('form')).submit();
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//*[@id="expand"]')), TEST_TIMEOUT);
        }).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(by.xpath('//*[@id="expand"]'))), TEST_TIMEOUT);
        }).then(function () {
            callback();
        });
    }

    function logout(callback) {
        browser.get('https://' + app.fqdn).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//*[@id="expand"]')), TEST_TIMEOUT);
        }).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(by.xpath('//*[@id="expand"]'))), TEST_TIMEOUT);
        }).then(function () {
            return browser.findElement(by.xpath('//*[@id="expand"]')).click();
        }).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(by.id('logout'))), TEST_TIMEOUT);
        }).then(function () {
            return browser.findElement(by.id('logout')).click();
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//input[@name="user"]')), TEST_TIMEOUT);
        }).then(function () {
            callback();
        });
    }

    function closeWizard(callback) {
        browser.get('https://' + app.fqdn).then(function () {
            return browser.wait(until.elementLocated(by.id('closeWizard')), TEST_TIMEOUT);
        }).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(by.id('closeWizard'))), TEST_TIMEOUT);
        }).then(function () {
            return browser.findElement(by.id('closeWizard')).click();
        }).then(function () {
            return browser.wait(until.elementLocated(by.id('filestable')), TEST_TIMEOUT);
        }).then(function () {
            // give it some time to save
            return browser.sleep(5000);
        }).then(function () {
            callback();
        });
    }

    function listUsers(callback) {
        browser.get('https://' + app.fqdn + '/index.php/settings/users').then(function () {
            return browser.wait(until.elementLocated(by.xpath('//th[text()="admin"]')), TEST_TIMEOUT); // should see admin user
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//th[text()="' + username + '"]')), TEST_TIMEOUT);
        }).then(function () {
            callback();
        }); // should see external user
    }

    function getContact(callback) {
        browser.get('https://' + app.fqdn + '/index.php/apps/contacts').then(function () {
            // give it some time to save
            return browser.sleep(5000);
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//div[text()="' + CONTACT_NAME + '"]')), TEST_TIMEOUT);
        }).then(function () {
            callback();
        });
    }

    function apiLogin(callback) {
        superagent.get('https://' + app.fqdn + '/ocs/v1.php/cloud/users/' + username).auth(username, password).end(function (error, result) {
            if (error !== null) return callback(error);
            if (result.statusCode !== 200) return callback('Status code: ' + result.statusCode);

            callback();
        });
    }

    function enableContacts(done) {
        var installButtonXpath = app.manifest.version.startsWith('1.2') ? '//span[text()="install"]' : '//button[text()="install"]';
        var uninstallButtonXpath = app.manifest.version.startsWith('1.2') ? '//span[text()="uninstall"]' : '//button[text()="uninstall"]';

        browser.get('https://' + app.fqdn + '/index.php/apps/market/#/app/contacts').then(function () {
            return browser.wait(until.elementLocated(by.xpath(installButtonXpath)), TEST_TIMEOUT);
        }).then(function () {
            var button = browser.findElement(by.xpath(installButtonXpath));
            return browser.executeScript('arguments[0].scrollIntoView(true)', button);
        }).then(function () {
            return browser.findElement(by.xpath(installButtonXpath)).click();
        }).then(function () {
            // enable contacts
            return browser.wait(until.elementLocated(by.xpath(uninstallButtonXpath)), TEST_TIMEOUT);
        }).then(function () {
            done();
        });
    }

    function addContact(done) {
        browser.get('https://' + app.fqdn + '/index.php/apps/contacts').then(function () {
            return browser.sleep(4000);
        }).then(function () {
            // click new contact
            return browser.wait(until.elementLocated(by.className('app-content-list-button')), TEST_TIMEOUT);
        }).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(by.className('app-content-list-button'))), TEST_TIMEOUT);
        }).then(function () {
            return browser.findElement(by.className('app-content-list-button')).click();
        }).then(function () {
            // add new contact
            return browser.wait(until.elementLocated(by.xpath('//*[@id="details-fullName"]')), TEST_TIMEOUT);
        }).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(by.xpath('//*[@id="details-fullName"]'))), TEST_TIMEOUT);
        }).then(function () {
            return browser.findElement(by.xpath('//*[@id="details-fullName"]')).clear();
        }).then(function () {
            return browser.findElement(by.xpath('//*[@id="details-fullName"]')).sendKeys(CONTACT_NAME);
        }).then(function () {
            return browser.findElement(by.xpath('//*[@id="details-fullName"]')).sendKeys(webdriver.Key.TAB);
        }).then(function () {
            // give it some time to save
            return browser.sleep(8000);
        }).then(function () {
            done();
        });
    }

    xit('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get userid', function (done) {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        superagent.post('https://' + inspect.apiEndpoint + '/api/v1/developer/login').send({
            username: username,
            password: password
        }).end(function (error, result) {
            if (error) return done(error);
            if (result.statusCode !== 200) return done(new Error('Login failed with status ' + result.statusCode));

            var token = result.body.accessToken;

            superagent.get('https://' + inspect.apiEndpoint + '/api/v1/profile')
                .query({ access_token: token }).end(function (error, result) {
                if (error) return done(error);
                if (result.statusCode !== 200) return done(new Error('Get profile failed with status ' + result.statusCode));

                userId = result.body.id;
                done();
            });
        });
    });

    function uploadFile(done) {
        // was unable to upload the file correctly using node, too much time wasted...
        var cmd = util.format('curl --insecure --http1.1 -X PUT -u %s:%s "https://%s/remote.php/webdav/%s" --data-binary @"./%s"', username, password, app.fqdn, REMOTE_FILENAME, LOCAL_FILENAME);
        execSync(cmd);
        done();
    }

    it('install app', function () {
        execSync('cloudron install --new --wait --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', getAppInfo);

    it('check scheduler script', function () {
        var output = execSync('cloudron exec --app ' + app.id + ' -- ls -l ' + manifest.addons.scheduler.housekeeping.command);
        console.log(output);
        expect(output.indexOf('-rwxrwxr-x')).to.be.greaterThan(-1);
    });

    it('can login', login.bind(null, username, password));
    it('can close the wizard', closeWizard);
    it('can logout', logout);
    it('can upload file', uploadFile);
    it('can download previously uploaded file', testFileDownload);
    it('can api login', apiLogin);
    it('can login as admin', login.bind(null, adminUser, adminPassword));
    it('can close wizard', closeWizard);
    it('can list users', listUsers);
    it('can enable contacts app', enableContacts);
    it('can add contact', addContact);
    it('can get contact', getContact);
    it('can logout', logout);

    it('can restart app', function () {
        execSync('cloudron restart --wait --app ' + app.id);
    });

    it('can login', login.bind(null, username, password));
    it('can logout', logout);
    it('can admin login', login.bind(null, adminUser, adminPassword));
    it('can list users', listUsers);
    it('can get contact', getContact);
    it('can logout', logout);
    it('can download previously uploaded file', testFileDownload);
    it('can api login', apiLogin);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('restore app', function () {
        execSync('cloudron restore --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can login', login.bind(null, username, password));
    it('can logout', logout);
    it('can admin login', login.bind(null, adminUser, adminPassword));
    it('can list users', listUsers);
    it('can get contact', getContact);
    it('can logout', logout);
    it('can download previously uploaded file', testFileDownload);
    it('can api login', apiLogin);

    it('move to different location', function () {
        browser.manage().deleteAllCookies();
        execSync('cloudron configure --wait --location ' + LOCATION + '2 --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    });

    it('can login', login.bind(null, adminUser, adminPassword));
    it('can list users', listUsers);
    it('can logout', logout);
    it('can admin login', login.bind(null, adminUser, adminPassword));
    it('can get contact', getContact);
    it('can download previously uploaded file', testFileDownload);
    it('can api login', apiLogin);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    // test update
    it('can install app', function () {
        execSync('cloudron install --new --wait --appstore-id ' + app.manifest.id + ' --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', getAppInfo);
    it('can login', login.bind(null, username, password));
    it('can close the wizard', closeWizard);
    it('can logout', logout);
    it('can upload file', uploadFile);
    it('can download previously uploaded file', testFileDownload);
    it('can admin login', login.bind(null, adminUser, adminPassword));
    it('can close the wizard', closeWizard);
    it('can enable contacts app', enableContacts);
    it('can add contact', addContact);
    it('can get contact', getContact);
    it('can logout', logout);
    it('can update', function () {
        execSync('cloudron install --wait --app ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
    it('can admin login', login.bind(null, adminUser, adminPassword));
    it('can get contact', getContact);
    it('can logout', logout);
    it('can login', login.bind(null, username, password));
    it('can download previously uploaded file', testFileDownload);
    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
});
