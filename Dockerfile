FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617

RUN mkdir -p /app/code
WORKDIR /app/code

RUN apt-get update && apt-get install -y smbclient && rm -r /var/cache/apt /var/lib/apt/lists

# get owncloud source
RUN curl -L https://download.owncloud.org/community/owncloud-10.2.0.tar.bz2 | tar -xj --strip-components 1 -f -

# create config folder link to make the config survive updates
RUN rm -rf /app/code/config && ln -s /app/data/config /app/code/config && \
    mv /app/code/apps /app/apps_template && ln -s /app/data/apps /app/code/apps

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf
RUN a2disconf other-vhosts-access-log && a2enmod rewrite
COPY apache/owncloud.conf /etc/apache2/sites-enabled/owncloud.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

RUN crudini --set /etc/php/7.2/apache2/php.ini PHP upload_max_filesize 20G && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP post_max_size 20G && \
    crudini --set /etc/php/7.2/apache2/php.ini Session session.save_path /run/owncloud/sessions && \
    crudini --set /etc/php/7.2/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/7.2/apache2/php.ini Session session.gc_divisor 100

RUN sed -e 's/\(.*\)php_value upload_max_filesize.*/\1php_value upload_max_filesize 5G/' \
        -e 's/\(.*\)php_value post_max_size.*/\1php_value post_max_size 5G/' \
        -i /app/code/.htaccess

ADD start.sh cron.sh /app/

RUN chown -R www-data.www-data /app/code

CMD [ "/app/start.sh" ]
