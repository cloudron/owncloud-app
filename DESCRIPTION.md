### Overview

This app packages OwnCloud <upstream>10.0.10</upstream>.

OwnCloud is a file sync and share server. It provides access to
your data through a web interface, sync clients or WebDAV while providing a
platform to view, sync and share across devices easily - all under your control.

### Features
 * Mobile and desktop syncing
 * Responsive Design
 * Versioning and Undelete
 * Galleries
 * Activity Feed
 * File editing and preview support for PDF, images, text files, Open Document, Word files and more.
 * Smooth performance and easy user interface.
 * Fine-grained control over access to data and sharing capabilities by user and by group.
